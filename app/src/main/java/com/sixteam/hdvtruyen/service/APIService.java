package com.sixteam.hdvtruyen.service;

public class APIService {

    private static final String BASE_URL = "http://192.168.64.3/pro1121/api/";

    public static DataService getService(){

        return APIRetrofitClient.getClient(BASE_URL).create(DataService.class);
    }
}
