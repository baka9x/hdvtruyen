package com.sixteam.hdvtruyen.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface DataService {

    @FormUrlEncoded
    @POST("post/register.php")
    Call<ResponseBody> createUser(
            @Field("username") String username,
            @Field("password") String password,
            @Field("email") String email);

    @FormUrlEncoded
    @POST("post/login.php")
    Call<ResponseBody> loginUser(
            @Field("username") String username,
            @Field("password") String password
    );
    

}
