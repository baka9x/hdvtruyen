package com.sixteam.hdvtruyen.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sixteam.hdvtruyen.R;
import com.sixteam.hdvtruyen.fragment.CategoryFragment;
import com.sixteam.hdvtruyen.fragment.HomeFragment;
import com.sixteam.hdvtruyen.fragment.MoreFragment;
import com.sixteam.hdvtruyen.fragment.SearchFragment;
import com.sixteam.hdvtruyen.fragment.SocialFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                   new HomeFragment()).commit();
                    break;

                case R.id.navigation_category:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new CategoryFragment()).commit();
                    break;

                case R.id.navigation_search:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new SearchFragment()).commit();
                    break;

                case R.id.navigation_social:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new SocialFragment()).commit();
                    break;
                case R.id.navigation_more:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MoreFragment()).commit();
                    break;

            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //Default
        if (savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragment()).commit();
        }


    }

}
