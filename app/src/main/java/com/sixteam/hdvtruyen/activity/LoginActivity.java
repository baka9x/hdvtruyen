package com.sixteam.hdvtruyen.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.sixteam.hdvtruyen.R;
import com.sixteam.hdvtruyen.service.APIService;
import com.sixteam.hdvtruyen.service.DataService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText username;
    TextInputEditText password;
    Button btnLogin, btnClear;
    CheckBox checkSave;
    SharedPreferences data;
    TextView forgetPassword, tvRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);




        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();



        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Đăng nhập tài khoản");

        init();
        saveInfo();

        if (bundle != null) {
            String unameBundle = (String) bundle.get("USERNAME");
            String upasswordBundle = (String) bundle.get("PASSWORD");
            username.setText(unameBundle);
            password.setText(upasswordBundle);
        }

        //Xử lý click
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username.setText("");
                password.setText("");
                checkSave.setChecked(false);
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                intent.putExtra("USERNAME", username.getText().toString().trim());
                startActivity(intent);
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String txtUsername = username.getText().toString().toLowerCase().trim();
                final String txtPassword = password.getText().toString().trim();


                if (TextUtils.isEmpty(txtUsername)) {
                    username.setError("Tên tài khoản không được bỏ trống");
                }else if(TextUtils.isEmpty(txtPassword)){
                    password.setError("Mật khẩu không được bỏ trống");
                }
                else {


                    DataService dataService = APIService.getService();

                    Call<ResponseBody> callback = dataService.loginUser(txtUsername, txtPassword);
                    callback.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            //Check username
                            startActivity(intent);
                            Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();

                            SharedPreferences.Editor editor = data.edit();
                            if (checkSave.isChecked()) {
                                //Luu tru thong tin vao file

                                editor.putString("username", txtUsername);
                                editor.putString("password", txtPassword);

                            }
                            editor.putBoolean("save_information", true);
                            editor.apply();
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });






                }

            }
        });




    }

    public void init(){
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btnLogin);
        btnClear = findViewById(R.id.btnClear);
        checkSave = findViewById(R.id.checkSave);
        forgetPassword = findViewById(R.id.forgetPassword);
        tvRegister = findViewById(R.id.tvRegister);
    }
    public void saveInfo(){
        data = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = data.getBoolean("save_information", false);
        if (dataSave) {
            username.setText(data.getString("username", ""));
            password.setText(data.getString("password", ""));
            checkSave.setChecked(true);
        }
    }
}
