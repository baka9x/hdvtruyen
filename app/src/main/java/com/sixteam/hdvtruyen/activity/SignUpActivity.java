package com.sixteam.hdvtruyen.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.sixteam.hdvtruyen.R;
import com.sixteam.hdvtruyen.service.APIRetrofitClient;
import com.sixteam.hdvtruyen.service.APIService;
import com.sixteam.hdvtruyen.service.DataService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    TextInputEditText username,password, rePassword, email;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txtUsername = username.getText().toString().toLowerCase().trim();
                String txtPassword = password.getText().toString().trim();
                String txtRePassword = rePassword.getText().toString().trim();
                String txtEmail = email.getText().toString().trim();

                //Check validatetion
                if (TextUtils.isEmpty(txtUsername)) {
                    username.setError("Bạn chưa nhập username");

//                }else if (userDAO.isDupicateUsername(txtUsername)){
//                    username.setError("Tên đăng nhập đã được đăng ký");

                }else if (txtUsername.length() < 4){
                    username.setError("Tên đăng nhập từ 4 ký tự đến 15 ký tự");

                }else if (txtUsername.length() > 15){
                    username.setError("Tên đăng nhập quá dài");

                }else if (txtUsername.contains(" ")){
                    username.setError("Tên đăng nhập có khoảng cách");

                }else if (TextUtils.isEmpty(txtEmail)){
                    email.setError("Bạn chưa nhập địa chỉ email");

//                }else if (!userDAO.isEmailValid(txtEmail)){
//                    email.setError("Email không đúng định dạng");
//
//                }else if (userDAO.isDupicateEmail(txtEmail)){
//                    email.setError("Email đã được đăng ký");

                }else if (TextUtils.isEmpty(txtPassword)){
                    password.setError("Bạn chưa nhập mật khẩu");

                }else if (txtPassword.length() < 6 || txtPassword.length() > 18) {
                    password.setError("Mật khẩu độ dài từ 6 đến 18 ký tự");

                }else if ( TextUtils.isEmpty(txtRePassword)){
                    rePassword.setError("Bạn chưa nhập lại mật khẩu");

                }  else if (!txtPassword.equals(txtRePassword)) {
                    rePassword.setError("Mật khẩu nhập lại không đúng");

                }

                else {

                    //Xử lý đăng ký
                    DataService dataService = APIService.getService();
                    Call<ResponseBody> callback = dataService.createUser(txtUsername, txtPassword, txtEmail);
                    callback.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Toast.makeText(SignUpActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(SignUpActivity.this, "Đăng ký thất bại", Toast.LENGTH_SHORT).show();
                        }
                    });

                }


            }
        });


    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Đăng ký tài khoản");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Init
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        rePassword = findViewById(R.id.rePassword);
        email = findViewById(R.id.email);
        btnRegister = findViewById(R.id.btnRegister);
    }
}
